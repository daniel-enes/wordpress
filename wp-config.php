<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/0qWFjPXDphyNGnjySlk6Uay94vshpjw+dztijwRMW7VqYlKqHcUBW6dXFKYBWsm37iaRATL7YSaW7GBM1ELuw==');
define('SECURE_AUTH_KEY',  'd93Fj6FOdLtyGfDjJeOl+8cflW4Vh9xfd6W0UAQlgi6SwIv82ToHMvSmCg73BLtChUHlMTdGkid5SczPOjc6bQ==');
define('LOGGED_IN_KEY',    '1g5PR9IYJqxkO7PgVJ5viZi3k+KmvCzADEn+T9VE3wCBy9zff1IMsArm72eHtEd4/U0DRJ+4cMfHG5hJ84n8Hw==');
define('NONCE_KEY',        'MS9+jZTzMCdkCWq46BAeaNRFMrhwp29l+SOveQbPFASb9kpPuxKBe5P0CYLJDSsbUAsqtguJMA5w85dyATv7sA==');
define('AUTH_SALT',        'OPWfhSukHooa0jE5B2fdgVb8nJMJRldsQ2PyX2HMeBBG8udnDEhwR3QO8UefgsK6FNjY1nypwWJ4PhI1DvCKiw==');
define('SECURE_AUTH_SALT', 'Pxsh1RpGQvOum38BF31vPDTm9RRa3s+NbFyRlDIPGtrGK+ezCyJVxJyyjUxmTae9hsrpKTQt7wYdGLo/GlUYtA==');
define('LOGGED_IN_SALT',   'vIrB2MNPBhRR2XWKtk6YnjsUjfdK2LLtDBpT9ur13dBvGuaoZwZe6QXFzl8S1I1rLMZwIgem+VLtPNhm8i8Erw==');
define('NONCE_SALT',       'GEGLrTOX2ZdU0Zubvc5AjL+TTU3ITl5ZGNNq2EEHJbKyHiZWqc06yjAhVcF9Ed/tgLT25FZmL/e7pPPF09zuHw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
