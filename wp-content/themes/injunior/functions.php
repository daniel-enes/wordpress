<?php

function treinamento_scripts() {
    //Folhas de estilo
    wp_enqueue_style('treinamento-style', get_template_directory_uri().'/style.css');

    //Javascript
    //wp_enqueue_script('meuscript-script', get_template_directory_uri( ).'/js/script.js');
}

add_action( 'wp_enqueue_scripts', 'treinamento_scripts');

add_theme_support( 'title-tag' );
add_theme_support( 'menus' );

function custom_post_type_noticias() {
    register_post_type('noticia', array(

    )
    );
}
add_action('init', 'custom_post_type_noticias');
?>