<?php 
/*
Template name: Home Page
*/
get_header() 
?>

    <h1><?php the_title(); ?></h1>

    <? get_template_part( 'inc', 'section', array('key' => "Textão" )); ?>
    
    <h1>Esse aqui é a PAGE.php</h1>
    <?php 
        the_content();
        $imagem = get_field('imagem');
    ?>
<img src="<?php echo $imagem['url'];?>">

<p><?php the_field('texto'); ?></p>

<div><?php the_field('area_de_texto'); ?></div>


<?php get_footer() ?>