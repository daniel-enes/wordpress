<?php 
/*
Template name: Posts
*/

get_header();
?>

    <h1>Esse aqui é o HOME.php com os POSTS</h1>
    <div><? get_search_form(); ?></div>
    <ul>
    <?php
    if(have_posts()) : 
        while(have_posts()) : the_post();       
    ?>
     <li>
     <h3><a href="<? the_permalink(); ?>"><?php the_title() ?></a></h3>
     <p><? the_content();?></p>
     </li>
            

    
    <?php
    endwhile;
    ?>
    </ul>
    <?
    else : 
    ?>
        <p><?php esc_html_e( 'Não há posts' ); ?></p>
    <?php 
    endif; 
    echo paginate_links( );
    ?>    

<?php get_footer(); ?>