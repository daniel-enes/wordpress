<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    
	<meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Começo do wp_head()-->
    <?php wp_head(); ?>
    <!-- Fim do wp_head()-->
</head>

<body>
<?php wp_body_open(); ?>

<header>
<?php 
$args = array(
    'menu' => 'menu-principal',
    'container' => 'nav'
);

wp_nav_menu( $args );
?>
</header>
<main>